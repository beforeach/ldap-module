<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright 2014 the original author or authors
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.foreach.across.modules</groupId>
		<artifactId>ldap-module-project</artifactId>
		<version>1.0.1.RELEASE</version>
	</parent>

	<artifactId>ldap-module</artifactId>

	<name>${project.groupId}:${project.artifactId}</name>

	<dependencies>
		<dependency>
			<groupId>com.foreach.across</groupId>
			<artifactId>across-web</artifactId>
		</dependency>
		<dependency>
			<groupId>com.foreach.across.modules</groupId>
			<artifactId>properties-module</artifactId>
		</dependency>
		<dependency>
			<groupId>com.foreach.across.modules</groupId>
			<artifactId>entity-module</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>com.foreach.across.modules</groupId>
			<artifactId>admin-web-module</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>com.foreach.across.modules</groupId>
			<artifactId>user-module</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>com.foreach.across</groupId>
			<artifactId>across-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.foreach.libs</groupId>
			<artifactId>common-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.jtds</groupId>
			<artifactId>jtds</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.oracle</groupId>
			<artifactId>ojdbc6</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.ldap</groupId>
			<artifactId>spring-ldap-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-ldap</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.directory.server</groupId>
			<artifactId>apacheds-all</artifactId>
			<version>1.5.5</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<!-- This makes sure that Intellij also configures the querydsl JPA AnnotationProcessor -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<annotationProcessors>
						<annotationProcessor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor
						</annotationProcessor>
						<annotationProcessor>com.querydsl.apt.jpa.JPAAnnotationProcessor</annotationProcessor>
					</annotationProcessors>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>com.querydsl</groupId>
						<artifactId>querydsl-apt</artifactId>
						<version>${querydsl.version}</version>
						<classifier>jpa</classifier>
					</dependency>
					<dependency>
						<groupId>org.hibernate.javax.persistence</groupId>
						<artifactId>hibernate-jpa-2.1-api</artifactId>
						<version>1.0.0.Final</version>
					</dependency>
				</dependencies>
			</plugin>
			<plugin>
				<groupId>org.asciidoctor</groupId>
				<artifactId>asciidoctor-maven-plugin</artifactId>
				<version>1.5.0</version>
				<configuration>
					<sourceDirectory>${project.basedir}/src/doc</sourceDirectory>
					<sourceDocumentName>index.adoc</sourceDocumentName>
					<outputDirectory>${project.reporting.outputDirectory}/${project.version}/reference</outputDirectory>
					<backend>html</backend>
				</configuration>
				<executions>
					<execution>
						<id>output-html</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>process-asciidoc</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>